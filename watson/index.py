import json
import MySQLdb
from watson_developer_cloud import AlchemyLanguageV1
from watson_developer_cloud import WatsonException
# Import config for database connection
from config import config


# Run AlchemyLanguage for specified text(icon description)
def run_alchemy(text):
    api_key = "d924bb0716adde1b161b0927f42ef839bd2d9dd3"
    alchemy_language = AlchemyLanguageV1(api_key=api_key)
    combined_operations = ['keyword', 'entity']
    response = alchemy_language.combined(
        text=text,
        extract=combined_operations
    )
    return response

# Fetch information about icons: id and description
connection = MySQLdb.connect(
    host=config['host'],
    user=config['user'],
    passwd=config['passwd'],
    db=config['database']
)
cursor = connection.cursor()
query = """
SELECT
    ci.common_icon_id,
    ci.description_for_blind
FROM common_icon as ci
WHERE ci.description_for_blind IS NOT NULL
LIMIT 2100,100
"""
cursor.execute(query)
icons = cursor.fetchall()
connection.close()

# Read already exists data
data = {}
with open("data2.json", "r") as outfile:
    try:
        print("Read data")
        data = json.load(outfile)
    except ValueError as ve:
        print(ve.message)
        print("Create empty json for store results")
        data = {}

# Process icons
print("Process descriptions with Watson")
for icon in icons:
    icon_id = icon[0]
    if icon_id not in data:
        text = unicode(icon[1], errors='replace')
        try:
            alchemy = run_alchemy(text)
            data[icon_id] = {
                'alchemy': alchemy,
                'text': text
            }
        except WatsonException as we:
            print(we.message)
            break

# Save data
print("Save result to file")
with open("data2.json", "w") as outfile:
    json.dump(data, outfile)
